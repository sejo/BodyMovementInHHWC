int MIN_YEAR = 1970;
int MAX_YEAR = 2016;

void setup(){
	String[] filenames = loadStrings("searches.txt");

	// Assign one sub-array per filename
	// Assign one element of the array per year
	// Probably a better approach would be to use HashMap,
	// but let's keep it simple.
	// index+MIN_YEAR -> Year corresponding to that index
	// Year - MIN_YEAR -> Index corresponding to that Year
	int [][] yearscount = new int[filenames.length][MAX_YEAR-MIN_YEAR+1];

	// Repeat for each file
	for(int j=0; j<filenames.length; j++){
		String filename = filenames[j];

		// Variable to hold a parsed JSON file
		JSONArray entries;
	
		// Load a JSON file
		entries = loadJSONArray(filename+".json");
	
		// Key to look for
		String keyword = "Release date";
	
		// Variable that will hold each entry
		JSONObject entry;
	

	
		// Variable to look for the lesser year
		int minyear = 2016;
	
		// Counter for entries with a null value in the key
		int nullcounter = 0;
	
		// Go through each entry
		for(int i=0; i< entries.size(); i++){
			entry = entries.getJSONObject(i);
	
			// Check if the value for the keyboard is null before attempting to get it
			if(entry.isNull(keyword)){
			//	println("Null");
				nullcounter++;
			}
			else{
				// Get the date string formated as yyyy-mm-dd
				String date = entry.getString(keyword);
				// Get the three values
				String[] values = split(date,"-");
				// Convert the year to an integer
				int year = int(values[0]);
				
				// Increment by one the count of that year
				yearscount[j][year-MIN_YEAR] ++;
	
				// Search for the mininum year
				if(year<minyear){
					minyear = year;
				}
			//	println(year);
			}
	
		}
	
		/*
		println("Results for the search term:");
		println(filename);
		println(String.format("Total count: %d, Min year: %d, Null count: %d",entries.size(),minyear,nullcounter));
	
	
		println("Number of entries per year:");
		println(String.format("null\t%d",nullcounter));
		// Go through each year counter and print it
		for(int i=0;i<yearscount[j].length;i++){
			println(String.format("%d\t%d",i+MIN_YEAR,yearscount[j][i]));
	
		}
		*/
	
	}

	String text;

	text = "Year";
	for(int j=0; j<filenames.length; j++){
		text += String.format("\t%s",filenames[j]);
	}
	println(text);

	for(int i=0;i<yearscount[0].length;i++){
		text = String.format("%d",i+MIN_YEAR);

		for(int j=0; j<filenames.length; j++){
			text += String.format("\t%d",yearscount[j][i]);
		}

		println(text);

	}

	exit();


}

void draw(){


}
