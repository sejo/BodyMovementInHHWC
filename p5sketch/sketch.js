
var actions = ["walk","run","step","jump"];
var clips = [];

//var sequence = ["jump","walk","jump","jump","run"];
var sequence;
var currentSequence = [];
var currentsubclip;
var seqPointer;

var cm; // Clips Manager

var ngifs = {"walk":2, "run":2, "step":2,"jump":2};

var gifw,gifh;


var artistsData = [];

var currentArtist;

var dataLines;

function preload(){


	var a;
	for(var i=0; i<actions.length; i++){
		a = actions[i];
		clips[a] = new Clip(a,0);

		var list = [];
		var c;
		for(var n=0; n<ngifs[a]; n++){
			c = new Clip(a,n);
			list.push(c);
		}

		clips[a] = list;
	}


	dataLines = loadStrings("data/frequencies.txt");



}

function setup() {


	var c = createCanvas(1200,600);
	c.parent("canvas");
	background(0);

	seqPointer = 0;
	currentsubclip = 0;

	cm = new ClipsManager();

	gifw = 400;
	gifh = 300;


	//	RUN WALK STEP JUMP
	var tokens;
	for(var i=0; i<dataLines.length; i++){
		tokens = split(dataLines[i],",");
		artistsData.push(new Artist(tokens[0],int(tokens[1]),int(tokens[2]),int(tokens[3]),int(tokens[4])));


	}

	currentArtist = 0;

	generateSequence(artistsData[currentArtist]);



}

function generateSequence(artist){
	println(artist.name);
	var counter = 0;
	currentSequence = [];
	sequence = [];
	for(var i=0; i<actions.length; i++){
		counter = artist.numbers[actions[i]];
		for(var n=0; n<counter; n++){
			sequence.push(actions[i]);
			println(actions[i]);
		}
	}

		seqPointer = floor(random(sequence.length));
		currentSequence.push(sequence[seqPointer]);
		currentsubclip = 0;



}

function draw() {

	background(0);

	translate(0,50);

	if(clips[sequence[seqPointer]][currentsubclip].gif.totalFrames()>0){
//	if(cm.loaded()){

	image(clips[sequence[seqPointer]][currentsubclip].gif,20,80,gifw,gifh);

	/*
	fill(255,0,0);
	rect(0,0,gifw,gifh);
	*/

	fill(255);
	textSize(60);
	text(artistsData[currentArtist].name,gifw*0.15,25);

	textSize(42);
	text(sequence[seqPointer],gifw*0.75,70);

	textSize(15);
	text("press 'j' or 'k' to change the artist",gifw*0.15,gifh*1.35);

	var y = 50;
	var x = gifw*1.25;
	textSize(12);
	var textVertLimit = 30;
	for(var i=0; i<currentSequence.length; i++){
		if(i%textVertLimit == 0){
			y = 50;
		}
		text(currentSequence[i],gifw*1.25 + floor(i/textVertLimit)*50,y);
		y+=14;

	}

	if(clips[sequence[seqPointer]][currentsubclip].isLastFrame()){

		println("last frame");
		clips[sequence[seqPointer]][currentsubclip].gif.frame(1);
		clips[sequence[seqPointer]][currentsubclip].gif.pause();
		seqPointer = floor(random(sequence.length));
		currentsubclip = floor(random(ngifs[sequence[seqPointer]]));
//		seqPointer = (seqPointer+1)%sequence.length;
		println("next: "+seqPointer + "  subclip: "+currentsubclip);
		currentSequence.push(sequence[seqPointer]);
		clips[sequence[seqPointer]][currentsubclip].gif.frame(1);
		clips[sequence[seqPointer]][currentsubclip].gif.play();

		}
	}

  
}

function ClipsManager(){

	this.loaded = function(){
		var result = true;
		for(var i=0; i<clips.length; i++){
			if(!clips[i].gif.loaded()){
				println("not loaded "+i);
				result = false;
			}
		}
		return result;
	}

}


function Clip(name,num){
	this.name = name;
	this.filename = name+"-"+num+".gif";
	this.gif = loadGif("images/gif/"+this.filename);

	this.load = function(){
		this.gif = loadGif("images/gif/"+this.filename);
	}

	this.isLastFrame = function(){
		return this.gif.frame()==this.gif.totalFrames()-1;
	}


}


function Artist(name,nrun,nwalk,nstep,njump){
	this.name = name;
	this.numbers = {"run":nrun, "walk":nwalk, "step":nstep, "jump":njump };
	

}

function keyTyped(){
	if(key === 'j'){
		currentArtist = (currentArtist+1)%artistsData.length;
	}
	else if(key === 'k'){
		currentArtist = (artistsData.length+currentArtist-1)%artistsData.length;
	}
	generateSequence(artistsData[currentArtist]);

		clips[sequence[seqPointer]][currentsubclip].gif.frame(1);
		clips[sequence[seqPointer]][currentsubclip].gif.play();

}
